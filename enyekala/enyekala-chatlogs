#!/usr/bin/env bash

# Enyekala log download
# Copyright (C) 2022,2024 Fedja Beader

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

download_log()
{
	date="$1"
	file="$2"

	if [[ -z "$date" ]]; then
		TZ=UTC printf -v date "%(%F)T"
	fi

	if [[ -z "$file" ]]; then
		file=-
	fi

	printf -v post_data "date=%s&submit=Show+Log+From+Date" "$date"
	printf "Post data: %s\n" "$post_data"

	wget --compression=auto \
	     -O "$file" \
	     'https://arklegacy-server.net/chat.html' \
	     --post-data "$post_data"
}

download_logs()
{
	dir="$1"
	printf -v seconds "%(%s)T" -1
	[[ -e "$dir" ]] || mkdir -p "$dir"

	# The loop is interrupted at 2017-07-02.
	for ((i = 0; i < 100000; ++i)); do
		# yesterday
		(( seconds = seconds - 86400 ))

		# 'date=2022-07-20&submit=Show+Log+From+Date'
		printf -v date "%(%F)T" "$seconds"

		if [[ "$date" == "2017-07-02" ]]; then
			break # no logs at or before this date
		fi

		file_name="$dir/$date.log"
		if [[ ! -f "$file_name" ]]; then
			download_log "$date" "$file_name"
			sleep 3
		fi
	done
}

fix_logs_missing_hour()
{
	dir="$1"
	hour="$2"

	mapfile problem_files < <(grep -e "$hour:.*:.* UTC" logs/* --count | grep ':0')
	local -a rm_args
	for line in "${problem_files[@]}"; do
		file="${line%%:*}"
		if [[ -n "$file" && -f "$file" ]]; then
			rm_args+=("$file")
			printf "  found: %s\n" "$file"
		fi
	done

	if [[ "${#rm_args[@]}" -gt 0 ]]; then
		rm -i "${rm_args[@]}"
	fi
}

fix_logs()
{
	printf "Searching for all logs that do not include any first-hour messages (00:*)...\n"
	fix_logs_missing_hour "$1" 00
	printf "Searching for all logs that do not include any last-hour messages (23:*)...\n"
	fix_logs_missing_hour "$1" 23
}

case "$1" in
	--download)
		if [[ -n "$2" ]]; then
			download_logs "$2"
		else
			printf "Missing argument to %s\n" "$1"
		fi
		;;
	--fix-logs)
		if [[ -n "$2" ]]; then
			fix_logs "$2"
		else
			printf "Missing argument to %s\n" "$1"
		fi
		;;
	--latest)
		download_log
		;;
	*)
		printf "Usage: %s --latest | less\n" "$0"
		printf "       %s --download <dir>\n" "$0"
		printf "       %s --fix-logs <dir>\n" "$0"
esac
