-- Enyekala TNT hole size estimator
-- Licensed under the GNU Affero GPLv3

-- (Obviously) includes Enyekala source code under MIT license,
-- see mods/tnt/license.txt for more info.

math.randomseed(os.time())

vector = {}
function vector.length(v)
	return math.sqrt(v.x * v.x + v.y * v.y + v.z * v.z)
end
function vector.new(x,y,z)
	return { x = x, y = y, z = z}
end

-- from mods/tnt/tnt_boom.lua
function count_blown_nodes(radius)
	count = 0

	for z = -radius, radius do
		for y = -radius, radius do
			for x = -radius, radius do

				local r = vector.length(vector.new(x, y, z))
				local r2 = radius

				-- Roughen the walls a bit.
			 	if math.random(6+1) == 1 then
					r2 = radius - 0.8
				end

			 	if r <= r2 then
					count = count + 1
				end
			end
		end
	end
	return count
end

function tnt_radius(num_tnts)
	radius = 3 -- in init.lua
	radius = math.floor(radius * math.pow(num_tnts, 0.60))
	return radius
end


num_samples = 10
for num_tnts = 1,64 do
	radius = tnt_radius(num_tnts)

	avg = 0
	for sample = 1,num_samples do
		avg = avg + count_blown_nodes(radius)
	end
	avg = avg / num_samples

	per_tnt = avg / num_tnts
	print(string.format("#TNTs = %2d -> radius = %2d -> %8d nodes blown up on average(N=%d), with %4d per TNT used.",
	                    num_tnts, radius, avg, num_samples, per_tnt))
end
