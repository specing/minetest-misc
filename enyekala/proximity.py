#!/usr/bin/env python3

# Enyekala log analysis program, python rewrite
# Copyright (C) 2024 Fedja Beader

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import atexit
import datetime
import io
import math
import os
import re

realm_aliases = {
    "Overworld":        "Overworld",
    "Iceworld":         "Overworld",

    "Caverns":          "Overworld",
    "Underearth":       "Overworld",

    "Nether":           "Overworld",
    "Netherealm":       "Overworld",
    "Netherworld":      "Overworld",
    "Abyssal Plain":    "Overworld",
    "Abyssal Sea":      "Overworld",

    # Can be either outback or overworld? Will need two POIs.
    # Adding this line results in no POIs found.
#    None:               "",

    "Outback":          "Outback",
#    "Abyss":            "Outback", # Internal alias only.

    "Midfeld":          "Midfeld",
    "Jarkati":          "Jarkati",
    "Channelwood":      "Channelwood",
    "Naraxen":          "Naraxen",
}

parser = argparse.ArgumentParser()
parser.add_argument("path", help = "dir of logs to parse")
parser.add_argument("-v", "--verbose", action = "store_true",
                    help = "be more verbose")
parser.add_argument("-D", "--max-dist", default = 10000, type = int,
                    help = "exclude POIs farther than this")
parser.add_argument("-t", "--target", default = "Overworld:0,0,0",
                    help = "realm_name:x,y,z coordinate to use as origin for"
                         + " POIs search. Defaults to Overworld:0,0,0")

def get_default_max_count():
    try:
        ts = os.get_terminal_size()
        return int(ts.lines / 2) # Can be a float
    except OSError: # printing to file or something
        return 1_000_000

parser.add_argument("-C", "--max-count", default = get_default_max_count(),
                    type = int, help = "only print this many closest POIs."
                                     + " Defaults to terminal lines / 2")
parser.add_argument("-a", "--aliases", action = "store_true",
                    help = "print a map of player -> all known aliases."
                           + " Supresses normal output")
args = parser.parse_args()

debug_level = 0
if args.verbose:
    debug_level += 1

def debug(str):
    if debug_level > 0:
        print(str)


# Stores start & end
class ChallengeInfo:
    def __init__(self, start_poi):
        # POI reference containing the starting coordinate
        self.start_poi = start_poi
        # POI reference for the last coordinate output on this challenge
        self.end_poi = start_poi
        # info on how the challenge went. Options:
        # None - challenge incomplete or missing logs
        # "victory", "cancelled"
        # "failed" - end_poi might contain bonebox coords
        self.end_info = None

# Stores a per-player list of challenges
class Challenges(list):
    #def __init__(self):
    #    super()__init__(self)

    def add_start(self, poi):
        ci = ChallengeInfo(poi)
        self.append(ci)
        return ci

    def add_poi(self, poi):
        active = self.active()
        if active:
            active.end_poi = poi

    def add_ending(self, outcome, line):
        # Find latest challenge and "close" it
        if len(self) == 0:
            print("%s, but no matching challenge found? %s" % (outcome, line))
            # TODO: Add fake challenge and close it immidiatelly?
            return

        # Take last one
        challenge = self[len(self)-1]
        if challenge.end_info:
            print ("%s, but challenge at %s has already terminated with %s"
                   % (outcome, str(challenge.start_poi), challenge.end_info))
        else:
            challenge.end_info = outcome
        return challenge

    def add_victory(self, line):
        self.add_ending("victory", line)

    def add_cancel(self, line):
        self.add_ending("cancelled", line)

    def add_failure(self, line):
        self.add_ending("failure", line)

    def active(self):
        if len(self) > 0:
            challenge = self[len(self)-1]
            if challenge.end_info == None:
                return challenge
        return None

# A Point Of Interest somewhere on the map
class POI:
    def __init__(self, realm, x, y, z, player, filepath, line):
        self.realm = realm
        self.x = x
        self.y = y
        self.z = z
        self.player = player # The player this belongs to. Should never be None
        self.path = filepath
        self.line = line
        self.challenge = None # Related <player>'s challenge, if any

    # Careful, this will be meaningless if realms mismatch
    def distanceTo(self, rhs):
        return math.sqrt(   (rhs.x - self.x)**2
                        # Intentionally higher penalty as moving up/down is harder
                        + 2*(rhs.y - self.y)**2
                        +   (rhs.z - self.z)**2 )

    # Called by print() via str()
    def __str__(self):
        extra_info = ""
        if c := self.challenge:
            extra_info += "on challenge"
            if c.end_info:
                extra_info += ", ended with " + c.end_info
                if self != c.end_poi: # Else infinite recursion
                    pass # too noisy extra_info += " at " + str(c.end_poi)
                else:
                    extra_info += " here" # ended ... here
        return "(%s:%d,%d,%d) %s (%s)" % (self.realm, self.x, self.y, self.z, self.line, extra_info)


# A Point of Interest relative to some other point of interest (stores distance)
class RelativePOI:
    def __init__(self, distance, poi):
        self.distance = distance
        self.poi = poi

    def __str__(self):
        return "%8d %s" % (self.distance, self.poi)


# A List of points of interest
class POI_List (list):
    pass

# A (sorted) List of points of interest relative to a target POI
class Relative_POI_List(list):
    def __init__(self, target, pois, max_dist):
        super().__init__()
        self.target = target
        self.max_dist = max_dist

        if not max_dist:
            max_dist = 2**30

        for p in pois:
            if target.realm == p.realm or (target.realm == None or p.realm == None):
                dist = target.distanceTo(p)
                if dist <= max_dist:
                    self.append(RelativePOI(dist, p))

        def poi_sorter(rpoi):
            return rpoi.distance

        # Reverse the list, so the closest POIs are
        # immidiatelly visible when the whole list is printed
        self.sort(key = poi_sorter, reverse = True)


class Player:
    def __init__(self, name):
        self.name = name
        # TODO: join/quit times?
        self.challenges = Challenges()
        self.aliases = list()

    def add_alias(self, alias):
        self.aliases.append(alias)

# maps name -> Player
class Player_Dict(dict):
    #def __init__(self):
    #    super().__init__()

    # override dict's operator[] to create a new player if one does not
    # exist yet. .setdefault does not work as the default value is not
    # a constant.
    def __getitem__(self, name):
        try:
            p = super().__getitem__(name)
        except KeyError:
            p = Player(name)
            # Use __setitem__ to avoid infinite recursion via []
            self.__setitem__(name, p)
        return p


# Implements a two level multimap of alias(string) to Player
# First from alias to real name, then from real name to Player.
class Players(dict):
    def __init__(self):
        super().__init__()
        self.real_players = Player_Dict()
        atexit.register(self.cleanup)

    def cleanup(self):
        if args.aliases:
            # If desired, print all aliases
            for name, player in self.real_players.items():
                if len(player.aliases) > 0:
                    print ("%16s: %s" % (name, str(player.aliases)))

    def get(self, alias): # Get player object
        real_name = super().get(alias, alias)
        return self.real_players[real_name]

    # When someone reidentifies, the current name will always
    # be the current alias OR player name and target name will
    # always be a new alias.
    def reident_to(self, old_name, new_alias):
        player_name = super().get(old_name)
        if player_name != None:
            del self[old_name]
        else:
            player_name = old_name
        self[new_alias] = player_name
        self.get(player_name).add_alias(new_alias)
        debug("Reident: " + old_name + "->" + new_alias
              + ". Player: " + player_name + ". Active: " + str(self))

    # This time target name is always real player name
    def rename_to(self, old_alias, player_name):
        # find player by old alias
        #player_name = self[old_alias]
        # complication: sometimes alias is old and not current one.
        if old_alias in self:
            del self[old_alias]
        debug("Rename: " + old_alias + "->" + player_name + ". Active: " + str(self))

    def __str__(self):
        aliases = ""
        for k in self.keys():
            aliases += k + "->" + self[k] + ", "
        return aliases


POIs = POI_List()

def pattern_to_group(name, pattern):
    return "(?P<" + name + ">" + pattern + ")"

def optional_pattern(pattern):
    # ?: makes () behave as regular paranthesis
    return "(?:" + pattern + ")?"

def any_of_patterns(*patterns):
    full_pattern = "(?:" + patterns[0]
    for index in range(1, len(patterns)):
        full_pattern += "|" + patterns[index]
    return full_pattern + ')'

def number_pattern(min_digits, max_digits, signed = False):
    pattern = "[0-9]{" + str(min_digits)
    if max_digits != min_digits:
        pattern += "," + str(max_digits)
    return pattern + "}"
def signed_number_pattern(min_digits, max_digits):
    return "[-]?" + number_pattern(min_digits, max_digits)

# speed-up by precompiling patterns:
#before: real    0m14.105s user    0m13.813s sys     0m0.263s
#after:  real    0m8.087s  user    0m7.804s  sys     0m0.266s

# Find mentions of coordinates
# "in the Underearth at (Caverns: 19956,-15281,20176)" <- at or @ or Realm
realm_name_outer_pattern_c = re.compile(
      "in the " + pattern_to_group("realm", "[^ ]*")
    + " " + any_of_patterns("@", "at", "Realm") + " ")

coordinate_start_brace_pattern = '[\\(\\[]{1}'
coordinate_end_brace_pattern   = '[\\)\\]]{1}'
coordinate_component_pattern   = '[-]?[0-9]{1,5}'
coordinate_component_delim_pattern   = ',[ ]?'

realm_coordinates_pattern = (""
    + optional_pattern(pattern_to_group("realm", "[^:]*") + ": ?")
    + pattern_to_group("x", coordinate_component_pattern)
    + coordinate_component_delim_pattern
    + pattern_to_group("y", coordinate_component_pattern)
    + coordinate_component_delim_pattern
    + pattern_to_group("z", coordinate_component_pattern))

realm_coordinates_enclosed_pattern = (""
    # Adds 8s to processing time (+41%), used alternative that adds only ~0.5s
    #+ optional_pattern("in the "
    #  + pattern_to_group("realm2", "[^ ]*")
    #  + " " + any_of_patterns("@", "at") + " ")
    + coordinate_start_brace_pattern
    + realm_coordinates_pattern
    + coordinate_end_brace_pattern)

# Assumption (tested and found correct before this change):
# All mentions of coordinates have one+ related player names.
# and are of the following two patterns:
# +From logs it does not seem possible for a name to have a space in it
# Server: Player &lt;Zero&gt; has begun a test of skill in the Underearth at (-29200,-14871,-11111)!
# [2021/01/10, 10:37:15 UTC]    &lt;_ [Overworld: 4387,-13,-2255]&gt; bb
player_and_coordinates_pattern_c = re.compile(
    '&lt;' + pattern_to_group("pname", '[^& ]+')
    + any_of_patterns(' ', '&gt.*')
    + realm_coordinates_enclosed_pattern)

# Extract date
# [2017/07/22, 16:55:53 UTC]    ...

#date_box_pattern = '^\\[' + '([^]]+)' + '\\]'
num44_pattern = '([0-9]{4})'
num12_pattern = '([0-9]{1,2})'

date_time_pattern_c = re.compile(
    '\\['
    + num44_pattern + '/' + num12_pattern + '/' + num12_pattern
    # In the first logs, only the date was recorded.
    + optional_pattern(
      ', '
      + num12_pattern + ':' + '[0-9]{1,2}' + ':' + '[0-9]{1,2}'
#      + num12_pattern + ':' + num12_pattern + ':' + num12_pattern
      + ' UTC')
    + '\\]')

log_file_date_pattern_c = re.compile(
      "/" + pattern_to_group("year",  number_pattern(4,4))
    + "-" + pattern_to_group("month", number_pattern(2,2))
    + "-" + pattern_to_group("day",   number_pattern(2,2))
    + ".log"
    )

# It seems that player names are encoded in these two formats:
# From logs it does not seem possible for a name to have a space in it
# Server: Player &lt;Zero&gt; has begun a test of skill in the Underearth at (-29200,-14871,-11111)!
# [2021/01/10, 10:37:15 UTC]    &lt;_ [Overworld: 4387,-13,-2255]&gt; bb
def player_name_pattern(group_name):
    return "&lt;" + pattern_to_group(group_name, "[^&]+") + "&gt;"

#player_name_pattern = '&lt;' + '([^&[]+)[^&]*' + '&gt;'
#player_name_pattern = '&lt;' + '([^& ]+)' + '(?:(?: )|(?:&gt;))'
# Coordinates already come from other matching.
# Server: Player &lt;J2&gt; has begun a test of skill in the Iceworld at (-19715,-10,9267)!
challenge_start_pattern_c = re.compile(
    'Server: Player ' + player_name_pattern("pname")
    + ' has begun a test of skill in the ' + '([^ ]+)' + ' at '
    )

# Server: Player &lt;censored1&gt; has claimed victory in the Surface Colony!
# Server: Player &lt;censored2&gt;'s survival skill was tested and found wanting!
# Server: Player &lt;censored3&gt; canceled a Survival Challenge and went home.
#[2022/09/07, 15:12:10 UTC]    # Server: Player &lt;censored1&gt; is reidentified as &lt;censored2&gt;!
# <old alias or original name> reidentified as <new alias>
#[2022/09/07, 17:00:15 UTC]    # Server: Player &lt;censored2&gt; renamed to &lt;censored3&gt;!
# <old alias> renamed to <original name>
server_message_c = re.compile(
    "Server: Player " + player_name_pattern("pname")
    + any_of_patterns(
        pattern_to_group("victory", " has claimed victory in"),
        pattern_to_group("failure", "'s survival skill was tested and found wanting!"),
        pattern_to_group("cancel",  " canceled a Survival Challenge and went home."),
        " is reidentified as " + player_name_pattern("reident_name") + "!",
        " renamed to " + player_name_pattern("renamed_name") + '!',
        )
    )



# Mostly we need just the date component of date&time, which is usually
# available in log's filename already (does not apply for test case files)
# and so we can avoid parsing date out of individual lines
# (which are assumed to match the file's date).
# TODO: add a test mode for checking this assumption
class lazy_datetime:
    def __init__(self, line, path_date):
        self.line = line
        self.dt = None
        self.path_date = path_date

    def parse_line(self):
        if datematch := date_time_pattern_c.search(self.line):
            #('2024', '10', '28', '23', '59', '47')
            debug (datematch.groups())
            year   = int(datematch.groups()[0])
            month  = int(datematch.groups()[1])
            day    = int(datematch.groups()[2])
            #print ("Year: %d, path year: %d" % (year, path_year))
            #assert (year == path_year)
            # No appearance in the first logs, assume 0
            hour   = int(datematch.groups()[3] or "0")
            #minute = int(datematch.groups()[4] or "0")
            #second = int(datematch.groups()[5] or "0")

            self.dt = datetime.datetime(year, month, day, hour)
        else:
            raise Exception("no date in line %s" % line)

    def date(self):
        if self.path_date:
            return self.path_date

        if not self.dt:
            self.parse_line()
        return self.dt.date()

    def datetime(self):
        # There's only date in log file name.
        if not self.dt:
            self.parse_line()
        return self.dt


name2player = Players()

all_offsets_fixed_2019_02_08 = datetime.date(2019, 2, 8)
date_first_realm_2019_02_01 = datetime.date(2019, 2, 1)
date_2019_02_03 = datetime.date(2019, 2, 3)
date_2019_02_04 = datetime.date(2019, 2, 4)
first_ground_level_adjust_2019_02_03_06h = datetime.datetime(2019, 2, 3, 6)
second_ground_level_adjust_2019_02_03_08h = datetime.datetime(2019, 2, 3, 8)

def process_file(path):
    debug("Processing %s:" % path)

    path_date = None
    if path_date_match := log_file_date_pattern_c.search(path):
        path_year  = int(path_date_match.group("year"))
        path_month = int(path_date_match.group("month"))
        path_day   = int(path_date_match.group("day"))
        path_date = datetime.date(path_year, path_month, path_day)

    #f = io.FileIO(path)
    #f.text_encoding("utf-8")
    for line in io.open(path):
        # TODO: line contains newlines.
        debug (line)

        if match := player_and_coordinates_pattern_c.search(line):
            debug (match.groups())
            player_name = match.group("pname")
            player = name2player.get(player_name)
            # 1 is realm with :
            realm = match.group("realm")
            x = int(match.group("x"))
            y = int(match.group("y"))
            z = int(match.group("z"))

            lazy_dt = lazy_datetime(line, path_date)
            date = lazy_dt.date()

            if realm == None:
                if date < date_first_realm_2019_02_01:
                    realm = "Overworld"
                if rmatch := realm_name_outer_pattern_c.search(line):
                    # Replace missing realm with outer one, if available:
                    realm = rmatch.group("realm")
                #if realm == None:
                #    print("Warning: realm unknown in %s" % line)
            else:
                # TODO: second part of realm in marked chat is claim name, use it somehow?
                realm = realm.split("/", 1)[0]

            real_realm = realm_aliases.get(realm)
            if real_realm == None and realm != None:
                print ("Warning: realm alias %s is not known in %s" % (realm, line))

            # See test-coordinate-offsets.log for explanation of this
            if date < all_offsets_fixed_2019_02_08:
                if y > 600:
                    real_realm = "Channelwood"
                    x = x - 2019
                    y = y - 3066
                    z = z + 1992
                elif real_realm == "Channelwood":
                    #realm_origin = {x=2019, y=3066, z=-1992},
                    x = x - 2019
                    z = z + 1992
                else:
                    #realm_origin = {x=-1067, y=-10, z=8930},
                    x = x + 1067
                    z = z - 8930
                    if date < date_2019_02_03:
                        if real_realm == None:
                            real_realm = "Overworld"
                        y = y + 10
                    elif date < date_2019_02_04:
                        dt = lazy_dt.datetime()
                        if dt < first_ground_level_adjust_2019_02_03_06h:
                            if real_realm == None:
                                real_realm = "Overworld"
                            y = y + 10
                        elif dt < second_ground_level_adjust_2019_02_03_08h:
                            y = y + 1

            #if real_realm == None:
            #    print ("realm unknown in %s" % line)

            poi = POI(real_realm,x,y,z, player, path, line)
            player.challenges.add_poi(poi)
            # We have coordinates, let's see what they're about
            if cs_match := challenge_start_pattern_c.search(line):
                debug ("challenge start groups: " + str(cs_match.groups()))
                player_name, realm_name = cs_match.groups()
                cplayer = name2player.get(player_name)
                crealm  = realm_aliases.get(realm_name)
                if cplayer != player:
                    Exception("challenge start player %s != POI player %s", cplayer.name, player.name)
                if crealm != real_realm:
                    Exception("challenge start realm %s != POI realm %s", crealm, real_realm)
                poi.challenge = player.challenges.add_start(poi)
            else:
                poi.challenge = player.challenges.active()

            POIs.append(poi)

        # No coordinates, but perhaps there might be a challenge victory or failure
        elif s_match := server_message_c.search(line):
            debug ("server message groups: " + str(s_match.groups()))
            player_name = s_match.group("pname")
            player = name2player.get(player_name)
            if   s_match.group("victory"):
                player.challenges.add_victory(line)
            elif s_match.group("failure"):
                player.challenges.add_failure(line)
            elif s_match.group("cancel"):
                player.challenges.add_cancel(line)
            elif re_name := s_match.group("reident_name"):
                name2player.reident_to(player_name, re_name)
            elif re_name := s_match.group("renamed_name"):
                name2player.rename_to(player_name, re_name)


# TODO: make a new iterable that advances to next log,
#       so it can be fed to a line reader?
def process_path(path):
    if os.path.isdir(path):
        # os.listdir  returns unsorted list of file/dir names
        # logs have to be chronologically sorted, otherwise detecting
        # challenge completion/failure/abort will not work.
        log_files = os.listdir(path)
        log_files.sort()
        for fname in log_files:
            new_path = os.path.join(path, fname)
            process_path(new_path)
    else:
        process_file(path)


# Try to parse target first, to spot issues early on.
target = None
tmatch = re.search(realm_coordinates_pattern, args.target)
if tmatch:
    debug ("target match groups: " + str(tmatch.groups()))
    realm = realm_aliases.get(tmatch.group("realm"))
    x = int(tmatch.group("x"))
    y = int(tmatch.group("y"))
    z = int(tmatch.group("z"))
    target = POI(realm, x, y, z, "script", "", "target given on command line")
    print ("Searching for POIs around " + str(target))

process_path(args.path)

if args.aliases:
    pass
elif target:
    rPOIs = Relative_POI_List(target, POIs, max_dist = args.max_dist)

    for i in range(max(0, len(rPOIs) - args.max_count), len(rPOIs)):
        print(rPOIs[i])
else:
    print("Target not given in valid form, aborting!")
